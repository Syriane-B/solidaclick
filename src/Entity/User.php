<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adress;

    /**
     * @ORM\Column(type="integer")
     */
    private $postCode;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $city;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Request", mappedBy="owner", orphanRemoval=true)
     */
    private $requestCreated;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Request", mappedBy="helper")
     */
    private $requestAnswered;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $phone;

    public function __construct()
    {
        $this->requestCreated = new ArrayCollection();
        $this->requestAnswered = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getAdress(): ?string
    {
        return $this->adress;
    }

    public function setAdress(string $adress): self
    {
        $this->adress = $adress;

        return $this;
    }

    public function getPostCode(): ?int
    {
        return $this->postCode;
    }

    public function setPostCode(int $postCode): self
    {
        $this->postCode = $postCode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return Collection|Request[]
     */
    public function getRequestCreated(): Collection
    {
        return $this->requestCreated;
    }

    public function addRequestCreated(Request $requestCreated): self
    {
        if (!$this->requestCreated->contains($requestCreated)) {
            $this->requestCreated[] = $requestCreated;
            $requestCreated->setOwner($this);
        }

        return $this;
    }

    public function removeRequestCreated(Request $requestCreated): self
    {
        if ($this->requestCreated->contains($requestCreated)) {
            $this->requestCreated->removeElement($requestCreated);
            // set the owning side to null (unless already changed)
            if ($requestCreated->getOwner() === $this) {
                $requestCreated->setOwner(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Request[]
     */
    public function getRequestAnswered(): Collection
    {
        return $this->requestAnswered;
    }

    public function addRequestAnswered(Request $requestAnswered): self
    {
        if (!$this->requestAnswered->contains($requestAnswered)) {
            $this->requestAnswered[] = $requestAnswered;
            $requestAnswered->setHelper($this);
        }

        return $this;
    }

    public function removeRequestAnswered(Request $requestAnswered): self
    {
        if ($this->requestAnswered->contains($requestAnswered)) {
            $this->requestAnswered->removeElement($requestAnswered);
            // set the owning side to null (unless already changed)
            if ($requestAnswered->getHelper() === $this) {
                $requestAnswered->setHelper(null);
            }
        }

        return $this;
    }

    public function getPhone(): ?int
    {
        return $this->phone;
    }

    public function setPhone(?int $phone): self
    {
        $this->phone = $phone;

        return $this;
    }
}
