<?php

namespace App\DataFixtures;

use App\DataFixtures\UserFixtures;
use App\Entity\Request;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class RequestFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $newRequest1 = new Request();
        $newRequest1->setContent('Pain, Lessive');
        $newRequest1->setOwner($this->getReference(UserFixtures::USER_REFERENCE . '1'));
        $newRequest1->setDate(new DateTime('@' . strtotime('now')));
        $newRequest1->setState(true);
        $newRequest1->setType('grocery');
        $manager->persist($newRequest1);

        $newRequest2 = new Request();
        $newRequest2->setContent('Doliprane, désinfectant');
        $newRequest2->setOwner($this->getReference(UserFixtures::USER_REFERENCE . '2'));
        $newRequest2->setDate(new DateTime('@' . strtotime('now')));
        $newRequest2->setState(true);
        $newRequest2->setType('pharmacy');
        $manager->persist($newRequest2);

        $newRequest3 = new Request();
        $newRequest3->setContent('pain complet, baguette');
        $newRequest3->setOwner($this->getReference(UserFixtures::USER_REFERENCE . '1'));
        $newRequest3->setHelper($this->getReference(UserFixtures::USER_REFERENCE.'3'));
        $newRequest3->setDate(new DateTime('@' . strtotime('now')));
        $newRequest3->setState(false);
        $newRequest3->setType('bread');
        $manager->persist($newRequest3);

        $manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return array(
            UserFixtures::class
        );
    }

}
