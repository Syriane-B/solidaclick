<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    public const USER_REFERENCE = 'user_';
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder){
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $newUser1 = new User();
        $newUser1->setFirstName('Emanuelle');
        $newUser1->setLastName('Fortu');
        $newUser1->setEmail('emmanuelle@fortu.com');
        $newUser1->setPassword(
            $this->passwordEncoder->encodePassword(
                $newUser1,
                'password'
            )
        );
        $newUser1->setAdress('9 rue Emile Zola');
        $newUser1->setPostCode(42000);
        $newUser1->setCity('Saint Etienne');
        $newUser1->setPhone(0605040302);
        $manager->persist($newUser1);
        $this->addReference(self::USER_REFERENCE . '1', $newUser1);

        $newUser2 = new User();
        $newUser2->setFirstName('Daniel');
        $newUser2->setLastName('Pautand');
        $newUser2->setEmail('daniel@pautand.com');
        $newUser2->setPassword(
            $this->passwordEncoder->encodePassword(
                $newUser2,
                'password'
            )
        );
        $newUser2->setAdress('1 boulevard Jean Jaurès');
        $newUser2->setPostCode(38000);
        $newUser2->setCity('Grenoble');
        $newUser2->setPhone(0605040302);
        $manager->persist($newUser2);
        $this->addReference(self::USER_REFERENCE . '2', $newUser2);

        $newUser3 = new User();
        $newUser3->setFirstName('Sandra');
        $newUser3->setLastName('Rolan');
        $newUser3->setEmail('sandra@rolan.com');
        $newUser3->setPassword(
            $this->passwordEncoder->encodePassword(
                $newUser3,
                'password'
            )
        );
        $newUser3->setAdress('2 place de gordes');
        $newUser3->setPostCode(38000);
        $newUser3->setCity('Grenoble');
        $newUser3->setPhone(0605040302);
        $manager->persist($newUser3);
        $this->addReference(self::USER_REFERENCE . '3', $newUser3);

        $manager->flush();
    }
}
