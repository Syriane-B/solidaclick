<?php

namespace App\Controller;

use App\Form\RequestFormType;
use App\Repository\RequestRepository;
use App\Repository\UserRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RequestController extends AbstractController
{
    private $entityManager;
    private $requestRepository;
    private $userRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        RequestRepository $requestRepository,
        UserRepository $userRepository
    )
    {
        $this->entityManager = $entityManager;
        $this->requestRepository = $requestRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @Route("/new-request", name="newRequest")
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function newRequest(Request $request): Response
    {
        //get the connected user
        $connectedUser = $this->getUser();

        //handle new request form
        $newRequest = new \App\Entity\Request();
        $form = $this->createForm(RequestFormType::class, $newRequest);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            //get information given by the form
            $newRequest = $form->getData();
            //set creation date
            $now = new DateTime();
            $newRequest->setDate($now);
            //set the user
            $newRequest->setOwner($connectedUser);
            //set the value of state on true
            $newRequest->setState(true);
            $this->entityManager->persist($newRequest);
            $this->entityManager->flush();
            return $this->redirect($request->getUri());
        }

        //display my requests
        $requests = $this->requestRepository->findRequestByUserId($connectedUser->getId());
        return $this->render('request/myRequest.html.twig', [
            'form' => $form->createView(),
            'requests' => $requests,
        ]);
    }

    /**
     * @Route("/delete-request/{requestId}", name="deleteRequest")
     * @param int $requestId
     * @return Response
     */
    public function deleteRequest(int $requestId)
    {
        //get the request to delete
        $requestToDelete = $this->requestRepository->find($requestId);
        //delete it
        $this->entityManager->remove($requestToDelete);
        $this->entityManager->flush();

        //redirect to my request page
        return $this->redirectToRoute('newRequest');
    }

    /**
     * @Route("/find-request", name="findRequest")
     */
    public function findRequest()
    {
        //get the connected user
        $connectedUser = $this->getUser();
        //get its department
        $dep = substr($connectedUser->getPostCode(), 0, 2);
        //find requests in the same departement
        $requests = $this->requestRepository->findRequestInTheSameDepartment($dep);
        //get helped request
        $helpedRequests = $connectedUser->getRequestAnswered();

        return $this->render('request/findRequest.html.twig', [
            'requests' => $requests,
            'helpedRequests' => $helpedRequests
        ]);
    }

    /**
     * @Route("/help/{requestId}", name="help")
     * @param int $requestId
     * @return Response
     */
    public function help(int $requestId)
    {
        //get the connected user
        $connectedUser = $this->getUser();
        //get the request to help
        $requestToHelp = $this->requestRepository->find($requestId);
        //add the helper to the request
        $requestToHelp->setHelper($connectedUser);
        //set the state of the request to false
        $requestToHelp->setState(false);
        $this->entityManager->persist($requestToHelp);
        $this->entityManager->flush();

        return $this->redirectToRoute('findRequest');
    }

    /**
     * @Route("/unhelp/{requestId}", name="unHelp")
     * @param int $requestId
     * @return Response
     */
    public function unHelp(int $requestId)
    {
        //get the connected user
        $connectedUser = $this->getUser();
        //remove the helper to the request
        $requestToUnHelp = $this->requestRepository->find($requestId);
        $connectedUser->removeRequestAnswered($requestToUnHelp);
        //set the state of the request to true
        $requestToUnHelp->setState(true);
        $this->entityManager->persist($requestToUnHelp);
        $this->entityManager->persist($connectedUser);
        $this->entityManager->flush();

        return $this->redirectToRoute('findRequest');
    }
}
