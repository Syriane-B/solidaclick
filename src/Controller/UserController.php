<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends AbstractController
{
    private $entityManager;
    private $userRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        UserRepository $userRepository
    )
    {
        $this->entityManager = $entityManager;
        $this->userRepository = $userRepository;
    }
    /**
     * @Route("/myProfile", name="myProfile")
     */
    public function myProfile()
    {
        $connectedUser = $this->getUser();

        return $this->render('user/myProfile.html.twig', [
            'user' => $connectedUser,
        ]);
    }

    /**
     * @Route("/delete-profile/{userId}", name="deleteProfile")
     * @param Request $request
     * @param int $userId
     * @return Response
     */
    public function deleteProfile(Request $request, int $userId)
    {
        //check if connected user match the user to delete
        $connectedUser = $this->getUser();
        if($connectedUser->getId() == $userId){
            $this->entityManager->remove($connectedUser);
            $this->entityManager->flush();
            //logout before redirecting
            $this->get('security.token_storage')->setToken(null);
            $request->getSession()->invalidate();

            //Create a flash message to inform the user the sucess of deleting its profile
            //$this->addFlash('success', 'Votre profil et vos publications ont bien été supprimés.');
            return $this->redirectToRoute('home');
        }
        return $this->redirectToRoute('myProfile');
    }

    /**
     * @Route("/profile/{userId}", name="userProfile")
     * @param int $userId
     * @return Response
     */
    public function userProfile(int $userId)
    {
        $user = $this->userRepository->find($userId);

        return $this->render('user/userProfile.html.twig', [
            'user' => $user
        ]);
    }

    /**
     * @Route("/modify-profile", name="modifyProfile")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     */
    public function modifyProfile(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $connectedUser = $this->getUser();

        $form = $this->createForm(RegistrationFormType::class, $connectedUser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $connectedUser->setPassword(
                $passwordEncoder->encodePassword(
                    $connectedUser,
                    $form->get('plainPassword')->getData()
                )
            );

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($connectedUser);
            $entityManager->flush();

            return $this->redirectToRoute('myProfile');
        }
        return $this->render('user/modifyProfilInfo.html.twig', [
            'registrationForm' => $form->createView()
        ]);
    }
}
