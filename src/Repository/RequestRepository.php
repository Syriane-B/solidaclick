<?php

namespace App\Repository;

use App\Entity\Request;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Request|null find($id, $lockMode = null, $lockVersion = null)
 * @method Request|null findOneBy(array $criteria, array $orderBy = null)
 * @method Request[]    findAll()
 * @method Request[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RequestRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Request::class);
    }


    public function findRequestInTheSameDepartment($dep)
    {
        return $this->createQueryBuilder('r')
            ->select('o','r')
            ->leftJoin('r.owner', 'o')
            ->where('o.postCode LIKE :dep')
            ->andWhere('r.state = true')
            ->setParameter('dep', "$dep%")
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
    }

    public function findRequestByUserId($userId)
    {
        return $this->createQueryBuilder('r')
            ->select('o','r')
            ->leftJoin('r.owner', 'o')
            ->where('o.id = :userId')
            ->setParameter('userId', $userId)
            ->getQuery()
            ->getResult();
    }
}
