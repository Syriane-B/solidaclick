# Hello ! Welcome to our Solida'Click project

1. To see it, clone the project
2. Rename .env.toCopy in .env and change if you want the user name and password
2. run `docker-compose exec php bash`
3. In this bash run `composer install`
4. run `npm install`

To start the project
1. run `docker-compose up`
2. run `ǹpm run dev`
3. inside this bash run `php bin/console d:m:m`
4. still in this bash run `php bin/console doctrine:fixtures:load`
5. go to `localhost:8282`
6. you can connect with thoses id : 'sandra@rolan.com' and 'password' or create your own account.
